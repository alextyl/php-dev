<?php

/**
 * Zend Server GUI password change script
 * 
 * This script should be executed by an administrator from the command line in
 * order to change the Zend Server Administration GUI password. 
 *
 * @copyright Copywrite (c) 2008 Zend Technologies, Inc. 
 * @version   $Id$
 */

// Define product name
define('ZWAS_PRODUCT', 'ZendServer');

// Define product GUI path (OS sepcific)
define('ZWAS_GUI_PATH', dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 
	'GUI' . DIRECTORY_SEPARATOR);
	
// Define zend-server-user.ini path
define('ZWAS_INI_FILE', ZWAS_GUI_PATH . 'application' . DIRECTORY_SEPARATOR . 
	'data' . DIRECTORY_SEPARATOR . 'zend-server-user.ini');

// Check that the ini file exists
if (! file_exists(ZWAS_INI_FILE)) {
	die_with_error(ZWAS_PRODUCT . " GUI configuration file is missing, please reinstall " . 
		ZWAS_PRODUCT);
}

// Check that we have permissions to write to zend-server-user.ini
if (! is_writable(ZWAS_INI_FILE)) {
	die_with_error(ZWAS_PRODUCT . " GUI configuration file is not writable. Perhaps you " . 
	                              "are not running as an administrator?");
}
	
// Load the INI file
$oldConfig = file_get_contents(ZWAS_INI_FILE);

// Change the password value
//$newConfig = preg_replace('/^(password\s*=\s*)\w+/m', '${1}0', $oldConfig);
//$newConfig = preg_replace('/^(password\s*=\s*)\w+/m', '${1}INSTALLATION_PLACEHOLDER_GUI_PASSWORD', $oldConfig);
$newConfig = preg_replace('/^(password\s*=\s*)[\w"]+/m', '${1}INSTALLATION_PLACEHOLDER_GUI_PASSWORD', $oldConfig);


if (! $newConfig) {
	die_with_error("Error: unable to set the new password, please reinstall " . 
		ZWAS_PRODUCT);
}

// Save the INI file
if (! file_put_contents(ZWAS_INI_FILE, $newConfig)) {
	die_with_error("Error: unable to save INI data to " . ZWAS_INI_FILE . ". You should reinstall " . 
		ZWAS_PRODUCT);
}

// End with a success message
echo ZWAS_PRODUCT . " administration GUI password has been reset." . PHP_EOL . 
	 "You should now log in to the GUI in order to set a new password.";

// End of script
	 
/**
 * Quit the script, presenting an error message and possibly an error
 * 
 * This function actually calls die() and never returns
 *
 * @param  string  $message Error message
 * @param  integer $code    Exit code
 * @return void
 */
function die_with_error($message, $code = 1)
{
	fprintf(STDERR, $message . PHP_EOL);
	exit((int) $code);
}
