﻿<html>

<head>
    <title>Волновой Алгоритм</title>
    <meta charset="UTF-8">
    <link href="style.css" rel="stylesheet">
    <script>
        function checkCurr(d) {
            if (window.event) {
                if (event.keyCode == 37 || event.keyCode == 39) return;
            }
            d.value = d.value.replace(/\D/g, '');
        }
    </script>
</head>

<body>
<div class="form">
    <form method="POST" name="FIELD_SETTING" id="SETTING">
        <label for="width">Ширина</label><br>
        <input type="number" id="width" value="<?= isset($_POST['width']) ? $_POST['width'] : 1 ?>" min="1" step="1"
               id="width" name="width" required>
        <label for="height">Высота</label><br>
        <input type="number" id="height" value="<?= isset($_POST['height']) ? $_POST['height'] : 1 ?>" min="1" step="1"
               id="height" name="height" required>
        <label for="height">Начальная точка Х</label><br>
        <input type="text" id="startPointX" name="startPoint[]"
               value="<?= isset($_POST['startPoint']) ? $_POST['startPoint'][0] : 1 ?>"/><br>
        <label for="height">Начальная точка Y</label><br>
        <input type="text" id="startPointY" name="startPoint[]"
               value="<?= isset($_POST['startPoint']) ? $_POST['startPoint'][1] : 1 ?>"/><br>
        <label for="height">Конечноая точка Х</label><br>
        <input type="text" id="endPointX" name="endPoint[]"
               value="<?= isset($_POST['endPoint']) ? $_POST['endPoint'][0] : 1 ?>"/><br>
        <label for="height">Конечная точка Y</label><br>
        <input type="text" id="endPointY" name="endPoint[]"
               value="<?= isset($_POST['endPoint']) ? $_POST['endPoint'][1] : 1 ?>"/><br><br>
        <input type="button" id="generation" name="generation" value="Сгенерировать"><br><br>
    </form>
</div>
<div class="Lee">
</div>


<script>
    var button = document.getElementById('generation');
    button.addEventListener('click', newsLoader);
    function newsLoader() {
        var request = new XMLHttpRequest(); //
        var formData = new FormData(document.forms.FIELD_SETTING); //
        request.open('POST', 'index.php', true); //
        request.onreadystatechange = function () {
            if (request.readyState == 4)
                if (request.status == 200) {
                    document.getElementsByClassName('Lee')[0].innerHTML = request.responseText; // иннет, нутыпонялда
                }
        }

        request.send(formData); //
    }
</script>


<?php
require_once("leeTable.php");
if (isset($_POST["width"])) {
    ob_clean();
    $abc = new LeeTable($_POST["width"], $_POST["height"], $_POST);
    echo $abc;
    //$abc->findPath($_POST);
    echo $abc($_POST);
    die();
}
?>
</body>

</html>