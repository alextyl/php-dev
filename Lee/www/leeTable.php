<?php
include_once "cell.php";

/**
 * Алгоритм Ли
 *
 * Матрица путевых клеток с реализованным методом нахождения кратчайщего пути алгоритмом Ли
 *
 * @author Alex Miklash <alextyl40000@gmail.com>
 * @version 2.0
 * @copyright Copyright (c) 2017, Alex Miklash
 */
class LeeTable
{

    /**
     * @var array() матрица путевых клеток из класса Cell
     */
    private $matrix = array();

    /**
     * Конструктор
     *
     * @param integer row - количество строк в матрице
     * @param integer col - количество стобцов в матрице
     * @param array ("startPoint" => array(x, y), "endPoint" => array(x, y)) - массив с координатами начальной и конечной точек
     */
    public function __construct($row = 5, $col = 5, $cells = array("startPoint" => array(2, 1), "endPoint" => array(4, 2)))
    {
        for ($x = 0; $x < $row; $x++) {
            for ($y = 0; $y < $col; $y++) {
                $this->matrix[$x][$y] = new Cell();
            }
        }
        if ($this->checkStartEnd($cells)) {
            $this->setStartEndCells($cells);
            $this->definingBarriers((int)$row * $col / 4);
        }
    }

    /**
     * Устанавливает в матрице начальную и конечную точки
     *
     * @param  array ("startPoint" => array(x, y), "endPoint" => array(x, y)) - массив с координатами начальной и конечной точек
     */
    private function setStartEndCells($cells)
    {
        $this->matrix[$cells["startPoint"][0]][$cells["startPoint"][1]]->setStart(true);
        $this->matrix[$cells["endPoint"][0]][$cells["endPoint"][1]]->setEnd(true);
    }

    /**
     * Устанавливает барьеры в матрице в случайных клетках
     *
     * @param integer countOfBarriers - количество барьеров
     */
    private function definingBarriers($countOfBarriers)
    {
        $rows = count($this->matrix) - 1;
        $cols = count($this->matrix[0]) - 1;
        while ($countOfBarriers > 0) {
            $randX = rand(0, $rows);
            $randY = rand(0, $cols);
            if (!$this->matrix[$randX][$randY]->getBarrier()
                && !$this->matrix[$randX][$randY]->getStart()
                && !$this->matrix[$randX][$randY]->getEnd()) {
                $this->matrix[$randX][$randY]->setBarrier(true);
                $countOfBarriers--;
            }
        }
    }

    /**
     * Находит кратчайший путь между начальной и конечной точками
     *
     * @param  array ("startPoint" => array(x, y), "endPoint" => array(x, y)) - массив с координатами начальной и конечной точек
     */
    public function findPath($cells)
    {
        $this->matrix[$cells["startPoint"][0]][$cells["startPoint"][1]]->setNumber(1);
        $numberOfStep = 1;
        do {
            if ($this->Step($numberOfStep)) {
                break;
            }
            $numberOfStep++;
        } while ($this->matrix[$cells["endPoint"][0]][$cells["endPoint"][1]]->getNumber() == 0);
        if ($this->matrix[$cells["endPoint"][0]][$cells["endPoint"][1]]->getNumber() != 0) {
            $this->drawingPath($cells);
        }
    }

    /**
     * Проверяет идентичность координат начальной и конечной точек
     *
     * @param  array ("startPoint" => array(x, y), "endPoint" => array(x, y)) - массив с координатами начальной и конечной точек
     * @return boolean - true если коордианаты не совпадают, false если координаты совпадают
     */
    private function checkStartEnd($cells)
    {
        if ($cells["startPoint"][0] == $cells["endPoint"][0] && $cells["startPoint"][1] == $cells["endPoint"][1]) {
            $this->matrix[$cells["startPoint"][0]][$cells["startPoint"][1]]->setPartOfPath(true);
            return false;
        }
        return true;
    }

    /**
     * Отрисовка существующего пути
     *
     * @param  array ("startPoint" => array(x, y), "endPoint" => array(x, y)) - массив с координатами начальной и конечной точек
     */
    private function drawingPath($cells)
    {
        $numberOfStep = $this->matrix[$cells["endPoint"][0]][$cells["endPoint"][1]]->getNumber();
        $currentCell[0] = $cells["endPoint"][0];
        $currentCell[1] = $cells["endPoint"][1];
        while ($numberOfStep > 2) {
            $currentCell = $this->drawingPathStep($currentCell, $numberOfStep--);
        }
    }

    /**
     * Отрисовка одной путевой клетки относительно текущей клетки
     *
     * @param  array () - координаты текущей рассматриваемой клекти
     * @param  integer numberOfStep - номер текущего шага
     * @return array() - координаты новой клетки, что станет текущей
     */
    private function drawingPathStep($currentCell, $numberOfStep)
    {
        for ($x = $currentCell[0] - 1; $x <= $currentCell[0] + 1; $x++) {
            for ($y = $currentCell[1] - 1; $y <= $currentCell[1] + 1; $y++) {
                if ($this->inField($x, $y)
                    && $this->matrix[$x][$y]->getNumber() == $numberOfStep - 1) {
                    $this->matrix[$x][$y]->setPartOfPath(true);
                    $currentCell[0] = $x;
                    $currentCell[1] = $y;
                    return $currentCell;
                }
            }
        }
    }

    /**
     * Один волновой шаг
     *
     * Перебирает все клетки в поисках тех, от которых волна будет идти дальше
     *
     * @param integer numberOfStep - номер текущего шага
     * @return boolean - true если путь не найден, но волны более не распространяются
     */
    private function Step($numberOfStep)
    {
        $deadlock = true;
        for ($x = 0; $x < count($this->matrix); $x++) {
            for ($y = 0; $y < count($this->matrix[0]); $y++) {
                if ($this->matrix[$x][$y]->getNumber() == $numberOfStep) {
                    $deadlock = $this->subStep($x, $y, $numberOfStep);
                }
            }
        }
        return $deadlock;
    }

    /**
     * Один волновой шаг
     *
     * Распространяет волну относительно клетки
     *
     * @param integer x - координата текущей точки по Ох
     * @param integer y - координата текущей точки по Оу
     * @param integer numberOfStep - номер текущего шага
     * @return boolean - true если путь не найден, но волны более не распространяются
     */
    private function subStep($x, $y, $numberOfStep)
    {
        for ($X = $x - 1; $X <= $x + 1; $X++) {
            for ($Y = $y - 1; $Y <= $y + 1; $Y++) {
                if ($this->inField($X, $Y)) {
                    if ($this->matrix[$X][$Y]->getBarrier() == false
                        && $this->matrix[$X][$Y]->getNumber() == 0) {
                        $this->matrix[$X][$Y]->setNumber($numberOfStep + 1);
                        $deadlock = false;
                    }
                }
            }
        }
        return $deadlock;
    }

    /**
     * Проверка вхождения индекса в границы матрицы
     *
     * @param integer x - координата текущей точки по Ох
     * @param integer y - координата текущей точки по Оу
     * @return boolean - true если точки входят в границы матрицы
     */
    private function inField($x, $y)
    {
        return $x >= 0 && $y >= 0 && $x < count($this->matrix) && count($this->matrix[$x]) > $y;
    }

    public function __invoke($cells)
    {
        $this->findPath($cells);
        return $this->__toString();
    }

    /**
     * Преобразует матрицу в HTML код в виде таблицы
     *
     * @return string - html-код матрицы
     */
    public function __toString()
    {
        $html = "<div class=\"table\">";
        for ($x = 0; $x < count($this->matrix); $x++) {
            $html = $html . "<div class=\"row\">";
            for ($y = 0; $y < count($this->matrix[0]); $y++) {
                $html = $html . $this->matrix[$x][$y];
            }
            $html = $html . "</div>";
        }
        $html = $html . "</div>";
        return $html;
    }
}

?>

