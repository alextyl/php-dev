﻿<?php

/**
 * Путевая клетка
 *
 * Путевая клетка используется в задачах нажодения путей
 *
 * @author Alex Miklash <alextyl40000@gmail.com>
 * @version 1.0
 * @copyright Copyright (c) 2017, Alex Miklash
 */

class Cell
{
    /**
     * @var boolean $start - является ли клетка началом пути
     * @var boolean $end - является ли клетка концом пути
     * @var boolean $barrier - является ли клетка проходимой
     * @var boolean $number - количество шагов сделанных для прихода в данную клетку
     * @var boolean $partOfPath - является ли клетка частью пути
     */
    private $start = false;            // является ли точка начальной
    private $end = false;            // является ли точка конечной
    private $barrier = false;        // является ли точка препядствием
    private $number = 0;        // расстояние от начальной точки
    private $partOfPath = false;    // является ли точка частью пути

    public function __construct($start = false, $end = false, $barrier = false, $number = 0, $partOfPath = false)
    {
        $this->start = $start;
        $this->end = $end;
        $this->barrier = $barrier;
        $this->number = $number;
        $this->partOfPath = $partOfPath;
    }

    public function __destruct()
    {

    }

    public function setStart($start)
    {
        $this->start = $start;
    }

    public function setEnd($end)
    {
        $this->end = $end;
    }

    public function setBarrier($barrier)
    {
        $this->barrier = $barrier;
    }

    public function setNumber($number)
    {
        $this->number = $number;
    }

    public function setPartOfPath($partOfPath)
    {
        $this->partOfPath = $partOfPath;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function getBarrier()
    {
        return $this->barrier;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function getPartOfPath()
    {
        return $this->partOfPath;
    }

    public function __toString()
    {
        if ($this->getPartOfPath()) {
            $html = "<div class=\"cell Path\">" . $this->getNumber() . "</div>";
        } else if ($this->getStart() && $this->getNumber() != 0) {
            $html = "<div class=\"cell Start\">" . $this->getNumber() . "</div>";
        } else if ($this->getEnd() && $this->getNumber() != 0) {
            $html = "<div class=\"cell End\">" . $this->getNumber() . "</div>";
        } else if ($this->getStart() && $this->getNumber() == 0) {
            $html = "<div class=\"cell Start\"></div>";
        } else if ($this->getEnd() && $this->getNumber() == 0) {
            $html = "<div class=\"cell End\"></div>";
        } else if ($this->getBarrier()) {
            $html = "<div class=\"cell Barrier\"></div>";
        } else if ($this->getNumber() != 0) {
            $html = "<div class=\"cell\">" . $this->getNumber() . "</div>";
        } else {
            $html = "<div class=\"cell\"></div>";
        }
        return $html;
    }
}

?>