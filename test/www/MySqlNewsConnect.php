<?php

define("HOST",     "localhost:31006"); // адрес сервера
define("DATABASE",    "news_schema"); // имя базы данных
define("USER", "root"); // имя пользователя
define("PASSWORD", ""); // пароль

class MySqlNewsConnect
{
    private $link;

    public function __construct()
    {
        $this->link = mysqli_connect(HOST, USER, PASSWORD, DATABASE)
        or die("Ошибка подключения к базе данных: " . mysqli_error($this->link));
    }

    public function rowsCount(){
        $sqlQuery = "SELECT COUNT(*) FROM items";
        $result = mysqli_query($this->link, $sqlQuery) or die("Ошибка выборки данных: " . mysqli_error($this->link));
        $row = mysqli_fetch_row($result);
        return $row[0];
    }

    public function select($colItems, $currentItem ){
        $sqlQuery = "SELECT * FROM items WHERE id >= $currentItem LIMIT $colItems";
        $result = mysqli_query($this->link, $sqlQuery) or die("Ошибка выборки данных: " . mysqli_error($this->link));
        if ($result){
            $html = "";
            for ($i = 0 ; $i < $result->num_rows; $i++)
            {
                $row = mysqli_fetch_row($result);
                $html .= "
                                <div class=\"new\">
                                    <h2>$row[1]</h2>
                                    <div class='description'>$row[2]</div>
                                    <br/>
                                    <a class='link' href=\"$row[3]\" target=\"_blank\">Подробнее</a>
                                </div>
                                ";
            }
        }
        mysqli_free_result($result);
        return $html;
    }

    public function __destruct()
    {
        mysqli_close($this->link);
    }
}

?>