﻿<html>
<head>
    <title> AJAX Example </title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php

require_once("MySqlNewsConnect.php");
$connect = new MySqlNewsConnect();
$template = "
    <div class=\"new\">
        <h2>#TITLE#</h2>
        <div class='description'>#DESCRIPTION#</div>
        <br/>
        <a class='link' href=\"#LINKMORE#\" target=\"_blank\">Подробнее</a>
    </div>
    ";

$newsOnPageCount = 3; // количество новостей на одной странице
$newsСount = $connect->rowsCount(); // количество новостей в базе
$currentPage = $_GET["page"]; // текущая страница
$pageCount = intval((($newsСount - 1) / $newsOnPageCount) + 1); // количество страниц
if (empty($currentPage) or $currentPage < 0) {
    $currentPage = 1;
}
if ($currentPage > $pageCount) {
    $currentPage = $pageCount;
}
$currentStartNew = $newsOnPageCount * $currentPage - $newsOnPageCount; // новость с которой нужно выводить пак
echo $connect->select($newsOnPageCount, $currentStartNew);

$leftPageCount = $currentPage - 1; // количество страниц левее текущей в пагинации
if ($leftPageCount > 3) {
    $leftPageCount = 3;
}

$rightPageCount = $pageCount - $currentPage; // количество страниц правее текущей в пагинации
if ($rightPageCount > 3) {
    $rightPageCount = 3;
}

if ($leftPageCount == 1){
    echo '<a href=index.php?page=' . 1 . ' id=link_'. 1 .' name="pagination_link">' ." ". 1 . " " . '</a>';
}

if ($leftPageCount == 2){
    echo '<a href=index.php?page=' . 1 . ' id=link_'. 1 .' name="pagination_link">' ." ". 1 . " " . '</a>';
    echo '<a href=index.php?page=' . intval($currentPage - 1) . ' id=link_'. intval($currentPage - 1) .' name="pagination_link">' ." ". intval($currentPage - 1) . " " . '</a>';
}

if ($leftPageCount == 3){
    echo '<a href=index.php?page=' . 1 . ' id=link_'. 1 .' name="pagination_link">' ." ". 1 . " " . ' </a>';
    echo '<a href=index.php?page=' . intval($currentPage / 2) . ' id=link_'. intval($currentPage / 2) .' name="pagination_link">' ." ". ' ... ' . " " . '</a>';
    echo '<a href=index.php?page=' . intval($currentPage - 1) . ' id=link_'. intval($currentPage - 1) .' name="pagination_link">' ." ". intval($currentPage - 1) . " " . '</a>';
}

echo '<a href=index.php?page=' . intval($currentPage) . ' id=link_currentPage name="pagination_link">' ." ". intval($currentPage) . " " . '</a>';

if ($rightPageCount == 1){
    echo '<a href=index.php?page=' . intval($pageCount) . ' id=link_'. intval($pageCount) .' name="pagination_link">' ." ". intval($pageCount) . " " . '</a>';
}

if ($rightPageCount == 2){
    echo '<a href=index.php?page=' . intval($currentPage + 1) . ' id=link_'. intval($currentPage + 1) .' name="pagination_link">' ." ". intval($currentPage + 1) . " " . '</a>';
    echo '<a href=index.php?page=' . intval($pageCount) . ' id=link_'. intval($pageCount) .' name="pagination_link">' ." ". intval($pageCount) . " " . '</a>';
}

if ($rightPageCount == 3){
    echo '<a href=index.php?page=' . intval($currentPage + 1) . ' id=link_'. intval($currentPage + 1) .' name="pagination_link">' ." ". intval($currentPage + 1) . " " . '</a>';
    echo '<a href=index.php?page=' . intval(($currentPage + ($pageCount - $currentPage) / 2) + 1) . ' id=link_'. intval(($currentPage + ($pageCount - $currentPage) / 2) + 1) .' name="pagination_link">' ." ". ' ... ' . " " . '</a>';
    echo '<a href=index.php?page=' . intval($pageCount) . ' id=link_'. intval($pageCount) .' name="pagination_link">' ." ". intval($pageCount) . " " . '</a>';
}



function getMacros($array)
{
    $macros = array();
    $keys = array_keys($array);
    foreach ($keys as $key) {
        $macros[] = "#" . strtoupper($key) . "#";
    }
    return $macros;
}

/*
$news = array(
    array("title" => "Да неужли?", "description" => "Почему бобёр способен отгрызть ногу или 20 причин поступить в ВУЗ", "linkMore" => "http://www.playground.ru/blogs/middle_earth_shadow_of_war/v_middle_earth_shadow_of_war_mozhno_perenesti_samogo_krutogo_vraga_iz_pervoj_chasti-257969/"),
    array("title" => "Важная новость", "description" => "Такая важнота, что её невозможно пропустить и необходимо клинуть по ссылке", "linkMore" => "https://oz.by/boardgames/more10497912.html"),
    array("title" => "Интересная новость", "description" => "А тебе интересно, что тут? Конечно, это ведь кликбейт! Скорее жми!", "linkMore" => "http://mathprofi.ru/zadachi_s_pryamoi_na_ploskosti.html"),
    array("title" => "Просто новость", "description" => "Просто описание к простой новости, ведь я простой человек", "linkMore" => "http://www.minsktrans.by/city/#minsk/bus"),
    array("title" => "Новость для профессии", "description" => "Разработка нового адронного колайдера завершилась образованием чёрной дыры", "linkMore" => "https://alogvinov.com/2017/07/v-rossii-vvedena-obyazatelnaya-registratsiya-dronov/"),
    array("title" => "Это лишь новости", "description" => "А теперь о погоде, будет пасмурно, тухло и минус пятнадцать", "linkMore" => "http://trikotazh.by/shop/kind/view/id/164638"),
    array("title" => "Но новость ли это?", "description" => "К такой новости возможно ли подобрать описание лучше, чем его отсутсвтие", "linkMore" => "http://7days.ru/news/egor-beroev-zayavil-chto-budet-prodavat-morozhenoe-vsyu-zhizn.htm"),
    array("title" => "Более чем новости", "description" => "Министерство юстиции Аргентины призвало к спокойствию и только спокойствию", "linkMore" => "https://news.mail.ru/incident/30304739/?frommail=1"),
    array("title" => "Майк Вазовский отец онлайнера", "description" => "И другие бракоразводные процессы в нашей рубрике", "linkMore" => "https://charter97.org/ru/news/2017/7/6/255456/"),
    array("title" => "Но тут.бай всё равно интереснее", "description" => "Согласно соц. опросу от вицом тут.бай интереснее онлайнера", "linkMore" => "https://news.tut.by/culture/550121.html"),
    array("title" => "Очередной успех", "description" => "ВЫ ТАКОГО НЕ ОЖИДАЛИ (А ДАЖЕ ЕСЛИ ОЖИДАЛИ, ТО НЕ ТАК)!", "linkMore" => "https://www.youtube.com/watch?v=_0vsWj-YpoQ"),
    array("title" => "Новость дня", "description" => "Скр-скр-скр, в мёртвых найках", "linkMore" => "https://www.google.by/search?q=sql+oracle+developer&ie=utf-8&oe=utf-8&gws_rd=cr&ei=9k9eWYjVJOaU6ASf_LQ4")
);*/
echo "<div class='news'>";
//Начальная адресс массива
$currentCount = 0;
//Проверяем, пришли ли данные
//echo $connect->select(3,0);
/*if (isset($_GET['currentCount'])) {//Если да
    $currentCount = (int)$_GET['currentCount'];//Считываем текущее количество элементов на странице
    ob_clean();//Очищаем всё что вывели раньше
    echo $connect->select(3,$currentCount);
    if ($currentCount + 3 > count($news))//Если число выводимых новостей превышает общий лимит
        die();//Выходим из программы
}*/
/*$macros = getMacros($news[0]);
for ($i = $currentCount; $i < $currentCount + 3; $i++) { //Выводим 3 текущих элемента
    echo str_replace($macros, $news[$i], $template);
}*/
//Если у нас есть входные данные, то на выводе новостей наш ответ заканчивается
if (isset($_GET['currentCount']))
    die();
echo "</div>";


?>


<!-- <button id="load">Загрузить</button>*/ -->
<script type="text/javascript">
    documet.getElementsByClassNam
    /*
        //Получаем кнопку со страницы
        var button = document.getElementById('load');
        //Назначаем ей событие клика
        button.addEventListener('click', newsLoader);

        //Функция загрузки новостей(клик)
        function newsLoader() {
            var request = new XMLHttpRequest();
            //Иницилизация данных
            request.open('GET', 'index.php?currentCount=' + document.getElementsByClassName('new').length, true);
            //Функция которая срабатывает при приходе ответа
            request.onreadystatechange = function () {
                if (request.readyState == 4)
                    if (request.status == 200) {
                        //Если пришёл пустой ответ(т.е. новости закончились)
                        if (request.responseText == '') {
                            //Убираем кнопку
                            button.style.display = 'none';
                            return;
                        }
                        //Иначе, добавляем пришедшие новости, к новостям на странице
                        document.getElementsByClassName('news')[0].innerHTML += request.responseText;
                    }
            }
            //Отправка данных
            request.send(null);
        }
        */
</script>


</body>
</html>